package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"time"
)

func main() {

	port := os.Getenv("PORT")
	if port == "" {
		port = "1234"
	}
	//http
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})
	fmt.Println("running..." + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))

	fmt.Println("halo dunia")
	/*
		//variable
		i := 0
		fmt.Println("print", i)
		var j int
		fmt.Println("print", j)

		//function
		a, _ := swap("satu", "dua")
		fmt.Println(a)

		//object=> struct
		//style1
		var p Product
		p.id = 1
		p.name = "pulsa"
		p.price = 10000

		p2 := &Product{
			id:    2,
			name:  "data",
			price: 20000,
		}
		fmt.Println(p2)
		nama := p2.getName()
		fmt.Println("product p2", nama)


		p3 := new(Product)
		p3.id = 4
		p3.name = "bakso"
		p3.price = 123344

		fmt.Println("product p3", p3.getName())

		//goroutine
		//go insertDB(1)
		//go insertDB(2)
		/*resDB := make(chan resultDb, 100)
		for i := 0; i < 100; i++ {
			go insertDB(i, resDB)
		}
		for i := 0; i < 100; i++ {
			out := <-resDB
			fmt.Println("callback", out.id, out.result)
		}
	*/

	/*go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()
	go leakyFunction()
	go leakyFunction2()

	*/

}

func leakyFunction() {
	s := make([]string, 1)
	for i := 0; i < 10000000; i++ {
		s = append(s, "magical pandas")
		if (i % 100000) == 0 {
			time.Sleep(500 * time.Millisecond)
		}
		fmt.Println("leaky1", i)
	}
}

func leakyFunction2() {

	s := make([]string, 1)
	for i := 0; i < 10000000; i++ {
		s = append(s, "magical pandas")
		if (i % 100000) == 0 {
			time.Sleep(1000 * time.Millisecond)
		}
		fmt.Println("leaky2", i)
	}
}

func tambah(a, b int) int {
	return a + b
}

type resultDb struct {
	id     int
	result bool
}

func insertDB(id int, result chan resultDb) {
	fmt.Println("insert DB", id)
	var res resultDb
	res.id = id
	res.result = true

	result <- res
}

type Product struct {
	id    int
	name  string
	price int
}

func (p Product) getName() string {
	return p.name
}

func swap(a, b string) (out1, out2 string) {

	return b, a
}
