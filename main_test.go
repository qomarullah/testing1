package main

import "testing"

func Benchmark(b *testing.B) {
	for i := 0; i < b.N; i++ {
		tambah(100000, 2000000)
	}
}
func Test_tambah(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{"test1", args{1, 2}, 3},
		{"test1", args{1, 4}, 5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tambah(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("tambah() = %v, want %v", got, tt.want)
			}
		})
	}
}
